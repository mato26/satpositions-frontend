import React, { Component } from 'react';
import styled from 'styled-components';

import Page from './components/Page';
import Header from './components/Header';

const AppDiv = styled.div`
  height: 100%;
`;

class App extends Component {
  render() {
    return (
      <AppDiv>
        <Header />
        <Page />
      </AppDiv>
    );
  }
}

export default App;
