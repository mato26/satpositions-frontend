export const SEARCH = 'SEARCH';
export const FETCH_SATELLITES = 'FETCH_SATELLITES';
export const FETCH_SATELLITES_SUCCESS = 'FETCH_SATELLITES_SUCCESS';
export const FETCH_SATELLITES_ERROR = 'FETCH_SATELLITES_ERROR';
export const FETCH_SATELLITES_CANCEL = 'FETCH_SATELLITES_CANCEL';
export const ADD_SATELLITE = 'ADD_SATELLITE';
export const REMOVE_SATELLITE = 'REMOVE_SATELLITE';
export const FETCH_SATELLITE_POSITIONS = 'FETCH_SATELLITE_POSITIONS';
export const FETCH_SATELLITE_POSITIONS_SUCCESS = 'FETCH_SATELLITE_POSITIONS_SUCCESS';
export const FETCH_SATELLITE_POSITIONS_ERROR = 'FETCH_SATELLITE_POSITIONS_ERROR';
export const HIDE_SEARCH_RESULTS = 'HIDE_SEARCH_RESULTS';
export const SHOW_HISTORY = 'SHOW_HISTORY';
export const HIDE_HISTORY = 'HIDE_HISTORY';
export const START_SPECTATE = 'START_SPECTATE';
export const STOP_SPECTATE = 'STOP_SPECTATE';

export const search = searchText => ({
  type: SEARCH,
  searchText,
});

export const hideSearchResults = () => ({
  type: HIDE_SEARCH_RESULTS,
});

export const fetchSatellites = searchText => ({
  type: FETCH_SATELLITES,
  searchText,
});

export const fetchSatellitesSuccess = response => ({
  type: FETCH_SATELLITES_SUCCESS,
  response,
});

export const fetchSatellitesError = response => ({
  type: FETCH_SATELLITES_ERROR,
  response,
});

export const fetchSatellitesCancel = () => ({
  type: FETCH_SATELLITES_CANCEL,
});

export const addSatellite = satellite => ({
  type: ADD_SATELLITE,
  satellite,
});

export const removeSatellite = satellite => ({
  type: REMOVE_SATELLITE,
  satellite,
});

export const fetchSatellitePositions = satellites => ({
  type: FETCH_SATELLITE_POSITIONS,
  satellites,
});

export const fetchSatellitePositionsSuccess = response => ({
  type: FETCH_SATELLITE_POSITIONS_SUCCESS,
  response,
});

export const fetchSatellitePositionsError = response => ({
  type: FETCH_SATELLITE_POSITIONS_ERROR,
  response,
});

export const showHistory = positions => ({
  type: SHOW_HISTORY,
  positions,
});

export const hideHistory = () => ({
  type: HIDE_HISTORY,
});

export const startSpectate = satellite => ({
  type: START_SPECTATE,
  satellite,
});

export const stopSpectate = () => ({
  type: STOP_SPECTATE,
});
