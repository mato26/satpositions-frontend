import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Map from './Map';

const StyledBody = styled.div`
  position: absolute;
  top: 70px;
  left: 303px;
  right: 0px;
  bottom: 0px;
`;

const Body = props => (
  <StyledBody>
    <Map
      satellites={props.satellites}
      showHistory={props.showHistory}
      hideHistory={props.hideHistory}
      history={props.history}
      center={props.center}
    />
  </StyledBody>
);

Body.propTypes = {
  satellites: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    noradId: PropTypes.string,
    positions: PropTypes.arrayOf(PropTypes.shape({
      altitude: PropTypes.string,
      latitude: PropTypes.string,
      longitude: PropTypes.string,
      timestamp: PropTypes.string,
    })),
  })).isRequired,
  showHistory: PropTypes.func.isRequired,
  hideHistory: PropTypes.func.isRequired,
  history: PropTypes.arrayOf(PropTypes.shape({
    altitude: PropTypes.string,
    latitude: PropTypes.string,
    longitude: PropTypes.string,
    timestamp: PropTypes.string,
  })).isRequired,
  center: PropTypes.shape({
    lat: PropTypes.string,
    lng: PropTypes.string,
  }).isRequired,
};

export default Body;
