import React from 'react';
import styled from 'styled-components';
import Label from './Label';

const HeaderDiv = styled.div`
  width: 100%;
  background: #3366cc;
  height: 70px;
  display: flex;
`;

const Header = () => (
  <HeaderDiv>
    <Label text="Satellite Position Viewer" />
  </HeaderDiv>
);
export default Header;
