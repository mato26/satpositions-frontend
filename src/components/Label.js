import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledLabel = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: 'Fredoka One', cursive;
  color: white;
  font-size: 35px;
  padding: 15px;
`;

const Label = props => <StyledLabel>{props.text}</StyledLabel>;

Label.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Label;
