import React from 'react';
import styled from 'styled-components';

import SearchContainer from '../containers/SearchContainer';
import SatelliteListContainer from '../containers/SatelliteListContainer';

const LeftBarDiv = styled.div`
  position: absolute;
  top: 70px;
  bottom: 0px;
  width: 300px;
  border-right: 3px solid #3366cc;
  max-height: 100%;
  overflow: hidden;
  display: flex;
  flex-direction: column;
`;

const LeftBar = () => (
  <LeftBarDiv>
    <SearchContainer />
    <SatelliteListContainer />
  </LeftBarDiv>
);

LeftBar.propTypes = {};

LeftBar.defaultProps = {};

export default LeftBar;
