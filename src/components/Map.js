import React from 'react';
import styled from 'styled-components';
import GoogleMapReact from 'google-map-react';
import PropTypes from 'prop-types';

import SatelliteMapPoint from './SatelliteMapPoint';

const StyledMap = styled(GoogleMapReact)`
  width: 100%;
`;

const HistoryPointStyled = styled.div`
  width: 6px;
  height: 6px;
  position: absolute;
  left: 3px;
  top: 3px;
  background: rgba(0, 0, 0, 0.8);
  border-radius: 50%;
`;

const Map = props => (
  <StyledMap
    bootstrapURLKeys={{ key: 'AIzaSyBpK13rG99VarUJVVVCpuoOcdbs3zdc9L8' }}
    defaultCenter={{
      lat: 0,
      lng: 0,
    }}
    center={props.center}
    defaultZoom={1}
  >
    {props.history !== undefined
      ? props.history.map(point => (
        <HistoryPointStyled key={point.timestamp} lat={point.latitude} lng={point.longitude} />
        ))
      : null}

    {props.satellites !== undefined
      ? props.satellites.map(satellite => (
        <SatelliteMapPoint
          lat={satellite.positions[0].latitude}
          lng={satellite.positions[0].longitude}
          theme={{ color: 'green', size: '20px' }}
          text={satellite.name}
          satellite={satellite}
          key={satellite.noradId}
          history={satellite.positions.slice(1)}
          showHistory={props.showHistory}
          hideHistory={props.hideHistory}
        />
        ))
      : null}
  </StyledMap>
);

Map.propTypes = {
  satellites: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    noradId: PropTypes.string,
    positions: PropTypes.arrayOf(PropTypes.shape({
      altitude: PropTypes.string,
      latitude: PropTypes.string,
      longitude: PropTypes.string,
      timestamp: PropTypes.string,
    })),
  })).isRequired,
  showHistory: PropTypes.func.isRequired,
  hideHistory: PropTypes.func.isRequired,
  history: PropTypes.arrayOf(PropTypes.shape({
    altitude: PropTypes.string,
    latitude: PropTypes.string,
    longitude: PropTypes.string,
    timestamp: PropTypes.string,
  })).isRequired,
  center: PropTypes.shape({
    lat: PropTypes.string,
    lng: PropTypes.string,
  }).isRequired,
};

export default Map;
