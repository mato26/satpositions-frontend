import React from 'react';
import styled from 'styled-components';

import LeftBar from './LeftBar';
import BodyContainer from '../containers/BodyContainer';

const StyledPageDiv = styled.div``;

const Page = () => (
  <StyledPageDiv>
    <LeftBar />
    <BodyContainer />
  </StyledPageDiv>
);

export default Page;
