import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import SatelliteListElement from './SatelliteListElement';

const StyledListDiv = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  left: 0px;
  width: 300px;
  top: 50px;
  bottom: 0px;
  z-index: 1;
`;

const SatelliteList = props => (
  <StyledListDiv>
    {props.satellites.map(satellite => (
      <SatelliteListElement
        key={satellite.noradId}
        satellite={satellite}
        removeSatellite={props.removeSatellite}
        startSpectate={props.startSpectate}
        stopSpectate={props.stopSpectate}
      />
    ))}
  </StyledListDiv>
);

SatelliteList.propTypes = {
  satellites: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    noradId: PropTypes.string,
  })),
  removeSatellite: PropTypes.func.isRequired,
  startSpectate: PropTypes.func.isRequired,
  stopSpectate: PropTypes.func.isRequired,
};

SatelliteList.defaultProps = {
  satellites: [],
};

export default SatelliteList;
