import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledElementDiv = styled.div`
  display: flex;
  z-index: 1;
  width: 100%;
  height: 40px;
  background: white;
  border-bottom: 1px solid grey;
  font-family: 'Open Sans', sans-serif;
  font-size: 18px;
  justify-content: center;
  align-items: center;
  :hover {
    background: #85a3e0;
    color: white;
  }
`;

const SatelliteListElement = (props) => {
  const removeSatellite = () => {
    props.removeSatellite(props.satellite);
  };

  const startSpectate = () => {
    props.startSpectate(props.satellite);
  };

  const stopSpectate = () => {
    props.stopSpectate();
  };

  return (
    <StyledElementDiv
      onMouseOver={startSpectate}
      onFocus={startSpectate}
      onMouseLeave={stopSpectate}
    >
      {props.satellite.name}
    </StyledElementDiv>
  );
};

SatelliteListElement.propTypes = {
  satellite: PropTypes.shape({
    name: PropTypes.string,
    noradId: PropTypes.string,
  }).isRequired,
  removeSatellite: PropTypes.func.isRequired,
  startSpectate: PropTypes.func.isRequired,
  stopSpectate: PropTypes.func.isRequired,
};

export default SatelliteListElement;
