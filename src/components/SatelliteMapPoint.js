import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Circle = styled.div`
  background: ${props => props.theme.color};
  width: ${props => props.theme.size};
  height: ${props => props.theme.size};
  color: ${props => props.theme.textColor};
  border-radius: 50%;
  position: absolute;
  bottom: -10px;
`;

const DivWrapper = styled.div`
  position: absolute;
`;

const BackgroundDiv = styled.div`
  position: absolute;
  left: -65px;
  width: 150px;
  height: 50px;
  border-radius: 10px;
  z-index: 2;
`;

const SatText = styled.p`
  position: absolute;
  top: -5px;
  left: 10px;
  width: auto;
  text-overflow: ellipsis;
  white-space: nowrap;
  padding-left: 10px;
  padding-right: 10px;
  text-align: center;
  font-size: 20px;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;

  background: rgba(0, 0, 0, 0.5);
  border-width: 2px;
  border-radius: 10px;
  transform: translateX(-50%);
`;

const SatelliteMapPoint = props => (
  <DivWrapper>
    <Circle theme={props.theme}>
      <BackgroundDiv
        onMouseOver={() => props.showHistory(props.history)}
        onMouseOut={() => props.hideHistory()}
        onBlur={() => props.hideHistory()}
        onFocus={() => props.showHistory(props.history)}
      />
    </Circle>
    <SatText>{props.text}</SatText>
  </DivWrapper>
);

SatelliteMapPoint.propTypes = {
  text: PropTypes.string,
  theme: PropTypes.shape({
    color: PropTypes.string,
    size: PropTypes.string,
    textColor: PropTypes.string,
  }),
  showHistory: PropTypes.func.isRequired,
  hideHistory: PropTypes.func.isRequired,
  history: PropTypes.arrayOf(PropTypes.shape({
    altitude: PropTypes.string,
    latitude: PropTypes.string,
    longitude: PropTypes.string,
    timestamp: PropTypes.string,
  })).isRequired,
};

SatelliteMapPoint.defaultProps = {
  text: '',
  theme: {
    color: 'black',
    size: '20px',
    textColor: 'black',
  },
};

/*
onMouseOver={() => {
          props.showHistory(props.history);
        }}
        onMouseOut={() => props.hideHistory()}
        onBlur={() => props.hideHistory()}
        onFocus={() => props.showHistory(props.history)}
        */

export default SatelliteMapPoint;
