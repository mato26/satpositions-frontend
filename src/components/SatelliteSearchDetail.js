import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const SatelliteSearchBtn = styled.button`
  display: flex;
  width: 100%;
  height: 40px;
  border: none;
  background: white;
  border-bottom: 1px solid grey;
  outline: none;
  font-family: 'Open Sans', sans-serif;
  font-size: 18px;
  justify-content: center;
  :hover {
    background: #29ae91;
    color: white;
  }
`;

class SatelliteSearchDetail extends React.Component {
  addSat = () => {
    this.props.addSat(this.props.satellite);
  };

  render() {
    return (
      <SatelliteSearchBtn onMouseDown={this.addSat}>{this.props.satellite.name}</SatelliteSearchBtn>
    );
  }
}

SatelliteSearchDetail.propTypes = {
  satellite: PropTypes.shape({
    name: PropTypes.string,
    noradId: PropTypes.string,
  }).isRequired,
  addSat: PropTypes.func.isRequired,
};

export default SatelliteSearchDetail;
