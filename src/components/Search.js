import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { Satellites } from '../proptypes/Satellite';
import SearchInput from './SearchInput';
import SearchResult from './SearchResult';

const SearchDiv = styled.div`
  border-bottom: 2px solid #3366cc;
  box-shadow: 0 0px 10px 0px #333;
  z-index: 2;
`;

const Search = props => (
  <SearchDiv>
    <SearchInput onSearchChange={props.search} hideResult={props.hideResults} />
    <SearchResult
      addSat={props.addSatellite}
      isLoading={props.isLoading}
      result={props.foundSatellites}
      showResults={props.showResults}
    />
  </SearchDiv>
);

Search.propTypes = {
  addSatellite: PropTypes.func.isRequired,
  search: PropTypes.func.isRequired,
  hideResults: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  showResults: PropTypes.bool.isRequired,
  foundSatellites: Satellites.isRequired,
};

export default Search;
