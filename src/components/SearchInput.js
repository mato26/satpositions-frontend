import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledSearchInputDiv = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 50px;
  border-bottom: 1px solid #3366cc;
`;

const StyledInput = styled.input`
  text-align: center;
  font-size: 25px;
  width: 100%;
  outline: none;
  border: none;
`;

/* const StyledResetButton = styled.button`
  width: 50px;
  height: 50px;
  font-size: 25px;
  background: white;
  font-family: Arial, Helvetica, sans-serif;
  color: #535e70;
  outline: none;
  border: none;
`; */

class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPlaceholder: true,
    };
  }

  onChangeValue = (e) => {
    const newVal = e.target.value;
    this.props.onSearchChange(newVal);
  };

  onGainedFocus = () => {
    this.setState({ showPlaceholder: false });
    this.onChangeValue({ target: { value: this.state.value } });
  };

  onLostFocus = () => {
    this.setState({ showPlaceholder: true });
    this.props.hideResult();
  };

  reset = () => {
    this.setState({ showPlaceholder: true });
  };

  render() {
    return (
      <StyledSearchInputDiv>
        <StyledInput
          placeholder={this.state.showPlaceholder ? 'Search...' : ''}
          value={this.props.value}
          onChange={this.onChangeValue}
          onFocus={this.onGainedFocus}
          onBlur={this.onLostFocus}
        />
      </StyledSearchInputDiv>
    );
  }
}

SearchInput.propTypes = {
  value: PropTypes.string.isRequired,
  onSearchChange: PropTypes.func.isRequired,
  hideResult: PropTypes.func.isRequired,
};

export default SearchInput;
