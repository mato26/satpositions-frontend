import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ReactLoading from 'react-loading';

import SatelliteSearchDetail from './SatelliteSearchDetail';

const SearchResultDiv = styled.div`
  overflow-y: auto;
  height: auto;
  background: white;
  max-height: calc(100vh - 122px);
`;

const StyledLoading = styled(ReactLoading)`
  width: 56px;
  margin: 0 auto;
  margin-bottom: 5px;
`;

const SearchResult = (props) => {
  if (props.isLoading) {
    return (
      <SearchResultDiv>
        <StyledLoading type="bars" color="black" />
      </SearchResultDiv>
    );
  }

  if (!props.result || !props.showResults) return <SearchResultDiv />;

  const satList = props.result.map(satellite => (
    <SatelliteSearchDetail addSat={props.addSat} key={satellite.noradId} satellite={satellite} />
  ));

  return <SearchResultDiv>{satList}</SearchResultDiv>;
};

SearchResult.propTypes = {
  isLoading: PropTypes.bool,
  showResults: PropTypes.bool.isRequired,
  result: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    noradId: PropTypes.string,
  })),
  addSat: PropTypes.func.isRequired,
};

SearchResult.defaultProps = {
  isLoading: false,
  result: [],
};

export default SearchResult;
