import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showHistory, hideHistory } from '../actions';
import Body from '../components/Body';

const mapStateToProps = state => ({
  satellites: state.satellites.satellitePositions,
  history: state.satellites.history,
  center: state.satellites.center,
});

const mapDispatchToProps = dispatch => ({
  showHistory: bindActionCreators(showHistory, dispatch),
  hideHistory: bindActionCreators(hideHistory, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Body);
