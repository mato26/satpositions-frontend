import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { removeSatellite, startSpectate, stopSpectate } from '../actions';
import SatelliteList from '../components/SatelliteList';

const mapStateToProps = state => ({
  satellites: state.satellites.satellites,
});

const mapDispatchToProps = dispatch => ({
  removeSatellite: bindActionCreators(removeSatellite, dispatch),
  startSpectate: bindActionCreators(startSpectate, dispatch),
  stopSpectate: bindActionCreators(stopSpectate, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SatelliteList);
