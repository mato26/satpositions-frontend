import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { search, addSatellite, hideSearchResults } from '../actions';
import Search from '../components/Search';

const mapStateToProps = state => ({
  searchValue: state.satellites.searchFieldString,
  foundSatellites: state.satellites.foundSatellites,
  isLoading: state.satellites.isFetchingSearch,
  showResults: state.satellites.showSearchResult,
});

const mapDispatchToProps = dispatch => ({
  search: bindActionCreators(search, dispatch),
  addSatellite: bindActionCreators(addSatellite, dispatch),
  hideResults: bindActionCreators(hideSearchResults, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
