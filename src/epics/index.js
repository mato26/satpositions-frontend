import Rx from 'rxjs';
import { combineEpics } from 'redux-observable';

import * as actions from '../actions';

const addTimeStamp = (currentTimestamp, array, endTimestamp) => {
  if (currentTimestamp > endTimestamp) {
    return array;
  }
  return addTimeStamp(currentTimestamp + 30, [...array, currentTimestamp + 30], endTimestamp);
};

const prepareTimestamps = (currentTimeStamp) => {
  const currentTimeStampInSeconds = Math.floor(currentTimeStamp / 1000);
  const from = currentTimeStampInSeconds - 1000;
  const to = currentTimeStampInSeconds + 1000;

  return addTimeStamp(from, [currentTimeStampInSeconds], to)
    .map(time => `"${time}"`)
    .join();
};

const prepareSatellitesQuery = satellites => satellites.map(sat => `"${sat.name}"`).join();

const prepareUrl = (satellites, timestamp) => {
  const urlPrefix = 'http://api.satpositions.com/graphql/graphql?query={satellites(';
  const urlSuffix = '){name,noradId,positions{latitude,longitude,altitude,timestamp}}}';

  const satellitesText = `name:[${prepareSatellitesQuery(satellites)}]`;
  const timestampsText = `timestamps:[${prepareTimestamps(timestamp)}]`;

  return `${urlPrefix + satellitesText},${timestampsText}${urlSuffix}`;
};

const search = action$ =>
  action$
    .ofType(actions.SEARCH)
    .mergeMap(action =>
      Rx.Observable.if(
        () => action.searchText !== undefined && action.searchText.length > 2,
        Rx.Observable.of(actions.fetchSatellites(action.searchText)),
        Rx.Observable.of(actions.fetchSatellitesCancel()),
      ));

const searchSatellites = action$ =>
  action$.ofType(actions.FETCH_SATELLITES).mergeMap(action =>
    Rx.Observable.ajax
      .getJSON(`http://api.satpositions.com/rest/search?query=${action.searchText}`)
      .map(res => actions.fetchSatellitesSuccess(res))
      .takeUntil(action$.ofType(actions.FETCH_SATELLITES))
      .catch(er => actions.fetchSatellitesError(er)));

const addSatellite = (action$, store) =>
  action$.ofType(actions.ADD_SATELLITE).switchMap(() =>
    Rx.Observable.interval(5000)
      .startWith(0)
      .takeUntil(action$.ofType(actions.ADD_SATELLITE))
      .mapTo(actions.fetchSatellitePositions(store.getState().satellites.satellites)));

const fetchSatellitesPositions = action$ =>
  action$.ofType(actions.FETCH_SATELLITE_POSITIONS).mergeMap(action =>
    Rx.Observable.ajax
      .getJSON(prepareUrl(action.satellites, Date.now()))
      .map(resp => actions.fetchSatellitePositionsSuccess(resp))
      .catch(er => actions.fetchSatellitePositionsError(er)));

export default combineEpics(search, searchSatellites, addSatellite, fetchSatellitesPositions);
