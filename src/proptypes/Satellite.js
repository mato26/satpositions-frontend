import PropTypes from 'prop-types';

export const Satellite = {
  name: PropTypes.string,
  noradId: PropTypes.string,
};

export const Satellites = {
  satellites: PropTypes.arrayOf(Satellite),
};
