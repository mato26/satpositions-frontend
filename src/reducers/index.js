import { combineReducers } from 'redux';
import satellites from './satellites';

export default combineReducers({ satellites });
