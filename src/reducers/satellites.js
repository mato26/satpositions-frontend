import * as actions from '../actions';

const satellites = (state = { satellites: [], satellitePositions: [] }, action) => {
  switch (action.type) {
    case actions.SEARCH:
      return {
        ...state,
        searchFieldString: action.searchText,
        showSearchResult: true,
      };
    case actions.FETCH_SATELLITES:
      return {
        ...state,
        isFetchingSearch: true,
        searchFieldString: action.searchText,
      };
    case actions.FETCH_SATELLITES_SUCCESS:
      return {
        ...state,
        isFetchingSearch: false,
        showSearchResult: true,
        foundSatellites: action.response.satellites,
      };
    case actions.FETCH_SATELLITES_ERROR:
      return {
        ...state,
        isFetchingSearch: false,
        showSearchResult: false,
        foundSatellites: [],
      };
    case actions.FETCH_SATELLITES_CANCEL:
      return {
        ...state,
        isFetchingSearch: false,
        foundSatellites: [],
      };
    case actions.HIDE_SEARCH_RESULTS:
      return {
        ...state,
        showSearchResult: false,
      };
    case actions.ADD_SATELLITE:
      return {
        ...state,
        satellites: [...state.satellites, action.satellite],
      };
    case actions.FETCH_SATELLITE_POSITIONS_SUCCESS:
      return {
        ...state,
        satellitePositions: action.response.satellites,
      };
    case actions.SHOW_HISTORY:
      return {
        ...state,
        history: action.positions,
      };
    case actions.HIDE_HISTORY:
      return {
        ...state,
        history: [],
      };
    case actions.START_SPECTATE: {
      const spectatingSatellite = state.satellitePositions.find(s => s.noradId === action.satellite.noradId);

      if (!spectatingSatellite) return { ...state };

      const newCenter = spectatingSatellite.positions[0];

      if (!newCenter) return { ...state };
      return {
        ...state,
        center: {
          lat: Number(newCenter.latitude),
          lng: Number(newCenter.longitude),
        },
        history: spectatingSatellite.positions.slice(1),
      };
    }
    case actions.STOP_SPECTATE:
      return {
        ...state,
        center: [],
        history: [],
      };
    default:
      return state;
  }
};

export default satellites;
